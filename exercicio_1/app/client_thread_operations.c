#include "mysocket.h"  

int main(int argc, char *argv[]) {

  TSocket sock;
  char *servIP, operation;
  unsigned short servPort;
  char str[100];
  int x, y, n, result;

  if (argc != 3) {
    ExitWithError("Usage: client <remote server IP> <remote server Port>");
  }
  
  servIP = argv[1];
  servPort = atoi(argv[2]);
  
  sock = ConnectToServer(servIP, servPort);	
	
	while(1) {
		printf("Use '+', '-', 'e' ou 'q'\n");
		do { 
    	scanf("%c", &operation);
		} while (operation == '\n');

    if (write(sock, &operation, 1) <= 0) {
      ExitWithError("Write Operation failed");
    }


    if (operation == '+' || operation == '-') {
      scanf("%d %d", &x, &y);

      if (write(sock, &x, sizeof(int)) <= 0) { 
        ExitWithError("Write result failed"); 
      }

      if (write(sock, &y, sizeof(int)) <= 0) { 
        ExitWithError("Write result failed"); 
      }

  		if (read(sock, &result, sizeof(int)) < 0) {
  			ExitWithError("Read operation failed");
  		} else {
  			printf("Resultado: %d\n", result);
  		}

    }
    else if (operation == 'e') {
      scanf(" %[^\n]",str);
      n = strlen(str);
      str[n] = '\n';

      if (WriteN(sock, str, ++n) <= 0) { 
        ExitWithError("WriteN() failed"); 
      }

      if (ReadLine(sock, str, 100) < 0) { 
        ExitWithError("ReadLine() failed"); 
      } 

			printf("%s",str);


    }  
    else if (operation == 'q') {
      break;
    }  
  }
	
  close(sock);
  return 0;
}
