#include "mysocket.h"  
#include <pthread.h>

#define NTHREADS 10
#define BUFSIZE 100

struct TArgs {
  TSocket cliSock;
};

int sum(int x, int y) {
	return x + y;
}

int subtract(int x, int y) {
	return x - y;
}

void * HandleRequest(void *args) {
	int x, y, result;
	char operation;
  char str[BUFSIZE];
	TSocket cliSock;
	
	cliSock = ((struct TArgs *) args)->cliSock;
	free(args);
	 
	while(1) {
		if (read(cliSock, &operation, 1) < 0) {
			ExitWithError("Read operation failed");
		} else {
			printf("%c\n", operation);
		}

    if (operation == 'e') {
      if (ReadLine(cliSock, str, BUFSIZE-1) < 0) { 
        ExitWithError("ReadLine() failed"); 
      } 
      
      printf("%s",str);   
 
      if (WriteN(cliSock, str, strlen(str)) <= 0)  { 
        ExitWithError("WriteN() failed"); 
      }  
    }
  
    else if (operation == '+' || operation == '-'){
      if (read(cliSock, &x, sizeof(int)) < 0) { 
      	ExitWithError("Read int x failed"); 
      } else {
        printf("Received first number = %d\n", x); 
      }
      
      if (read(cliSock, &y, sizeof(int)) < 0) {
        ExitWithError("Read int y failed"); 
      } else {
        printf("Received second number = %d\n", y); 
      }

      if (operation == '+') {
      		result = sum(x, y);
      }
      else if (operation == '-') {
      	result = subtract(x,y);
      }    
      if (write(cliSock, &result, sizeof(int)) <= 0) { 
        ExitWithError("Write result failed"); 
      }  
    }

    else if (operation == 'q') {
      break;
    }
	 	 
		
    

	}
  printf("Saindo da thread\n");
	close(cliSock);
	pthread_exit(NULL);
	
}

int main(int argc, char *argv[]) {
	TSocket srvSock, cliSock; 
	struct TArgs *args;
	pthread_t threads[NTHREADS];
	int tid = 0;
	
	if (argc != 2) {
		ExitWithError("Usage: server <local port>");
	}
	
	srvSock = CreateServer(atoi(argv[1]));

	while(1) {
		if (tid == NTHREADS){
			ExitWithError("number of threads is over");
		}
		
		cliSock = AcceptConnection(srvSock);
		
    if ((args = (struct TArgs *) malloc(sizeof(struct TArgs))) == NULL) {
    	ExitWithError("malloc() failed");
    }
    
    args->cliSock = cliSock;
		
		if (pthread_create(&threads[tid++], NULL, HandleRequest, (void *) args)) {
			ExitWithError("pthread_create() failed");
		}
		
	}

}
