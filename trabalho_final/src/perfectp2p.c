/*
 * Algorithm PerfectP2P
 */

#include "fairp2p.h"
#include "perfectp2p.h"
#include <unistd.h>
#include <string.h>
#define MAX_MESSAGES 30
#define MESSAGE_REPEAT 20

typedef struct {
  char ip[16];
  int id;
} Message;

int messageId = 0;

Message receivedMessages[MAX_MESSAGES];

int nextMessageReceived;

int nextMessageId() {
  return messageId++;
}

int retrieveMessageId(char *msg) {
  return atoi(msg);
}

char* retrieveMessage(char *msg) {

  char *separator;

  separator = strstr(msg, "|");

  return separator + 1;

}

int messageReceived(char *src, int id) {
  int i;
  for (i = 0; i < MAX_MESSAGES; i++) {
    Message msg = receivedMessages[i];

    if (0 == strcmp(msg.ip, src) && msg.id == id) {
      return 1;
    }
  }
  return 0;
}

void appendNewMessage(char *src, int id) {

  Message *msg = &receivedMessages[nextMessageReceived];
  strcpy(msg->ip, src);
  msg->id = id;

  nextMessageReceived++;
  if (nextMessageReceived == MAX_MESSAGES) {
    nextMessageReceived = 0;
  }
}

// Request PerfectP2P event: send
int pp2pSend (char *dest, char *msg) {

  int i;
  int bytes;
  char str[BUFSIZE];

  sprintf(str,"%d|%s", nextMessageId(), msg);

  for (i = 0;i < MESSAGE_REPEAT; i++) {
    bytes = fp2pSend(dest, str);
    usleep(1000 * 10);

  }

  return bytes;
}

// PerfectP2P init event
void pp2pInit(int pp2psendPort, int pp2precvPort) {
  nextMessageReceived = 0;

  fp2pInit(pp2psendPort, pp2precvPort);
}

void fp2pDelivery (char *src, char *msg) {
    char *message;
    int idMessage;
    message = retrieveMessage(msg);
    idMessage = retrieveMessageId(msg);
    if (!messageReceived(src, idMessage)) {
        pp2pDelivery(src, message);
        appendNewMessage(src, idMessage);
    }

}
