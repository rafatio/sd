#include <stdio.h>
#include <stdlib.h>
#include "perfectp2p.h"
#include "fairp2p.h"
#include <semaphore.h>
#include <time.h>

#define SENDPORT 4000
#define RECVPORT 4000

sem_t mutex;

void waitForEnter() {
  getchar();
}

void criticalSection() {
  printf("\nEntrando na SC\n");
  waitForEnter();

  printf("Saindo da SC\n");
}

void pp2pDelivery (char *ip, char *msg) {
  printf("Recebendo TOKEN do servidor\n");
  sem_post(&mutex);
}

int main(int argc, char **argv) {

  char ipServidor[30];

  strcpy(ipServidor, argv[1]);


  sem_init(&mutex, 0, -1);

  pp2pInit(SENDPORT, RECVPORT);

  waitForEnter();
  while(1) {

    printf("Enviando REQUEST para o servidor\n");
    pp2pSend(ipServidor, "REQUEST");

    sem_wait(&mutex);

    criticalSection();

    printf("Devolvendo TOKEN para o servidor\n");
    pp2pSend(ipServidor, "TOKEN");
    waitForEnter();
  }

  return 0;
}
