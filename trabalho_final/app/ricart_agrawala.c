#include <stdio.h>
#include <stdlib.h>
#include "perfectp2p.h"
#include "fairp2p.h"
#include <semaphore.h>

#define HELD 'H'
#define RELEASED 'R'
#define WANTED 'W'

#define MAX_NODES 10
#define IP_LENGTH 16

#define SENDPORT 4000
#define RECVPORT 4000

char state;
pthread_mutex_t lock;
int timeOwnReq;
int responsesWaiting;
int currentTime = 0;
int numberOfPeers;
char requestsQueue[MAX_NODES][IP_LENGTH];
int requestsQueueCount = 0;
char** nodeIds;
sem_t mutex;

char nodes[MAX_NODES][IP_LENGTH];

void waitForEnter() {
  getchar();
}

void broadcast(char* message) {
  int i;

  char msg[50];

  pthread_mutex_lock(&lock);
  currentTime++;
  pthread_mutex_unlock(&lock);

  timeOwnReq++;

  sprintf(msg, "%d|%s", currentTime, message);

  for (i = 0; i < numberOfPeers; i++) {
    printf("Enviando %s para %s\n", message, nodes[i]);
    pp2pSend(nodes[i], msg);
  }

}

void sendMessage(char* ip, char* message) {

  char msg[50];

  pthread_mutex_lock(&lock);
  currentTime++;
  pthread_mutex_unlock(&lock);

  printf("Enviando %s para %s\n", message, ip);

  sprintf(msg, "%d|%s", currentTime, message);

  pp2pSend(ip, msg);
}

int getLamportClock(char *msg) {
  return atoi(msg);
}

char* getContent(char *msg) {

  char *separator;

  separator = strstr(msg, "|");

  return separator + 1;

}

void criticalSection() {
  printf("\nEntrando na SC\n");
  waitForEnter();
  printf("Saindo da SC\n");
}

void pp2pDelivery (char *ip, char *msg) {
  int timeMsg;
  char* msgContent;

  timeMsg = getLamportClock(msg);
  msgContent = getContent(msg);

  printf("\nRecebendo %s de %s\n", msgContent, ip);

  pthread_mutex_lock(&lock);
  if (currentTime < timeMsg) {
    currentTime = timeMsg;
  }
  currentTime++;
  pthread_mutex_unlock(&lock);

  if (0 == strcmp(msgContent, "REQUEST")) {
    switch(state) {
      case RELEASED:
        printf("Estado atual: RELEASED\n");
        sendMessage(ip, "RESPONSE");
        break;

      case WANTED:
        printf("Estado atual: WANTED\n");
          if (timeMsg < timeOwnReq) {
            sendMessage(ip, "RESPONSE");
          } else {
            printf("Colocando %s na fila de requests\n", ip);
            strcpy(requestsQueue[requestsQueueCount], ip);
            requestsQueueCount++;
          }
        break;

      case HELD:
        printf("Estado atual: HELD\n");
        printf("Colocando %s na fila de requests\n", ip);
        strcpy(requestsQueue[requestsQueueCount], ip);
        requestsQueueCount++;
        break;
    }
  } else if (0 == strcmp(msgContent, "RESPONSE")) {
    responsesWaiting--;
    if (responsesWaiting == 0) {
      sem_post(&mutex);
    }
  }
}

int main(int argc, char **argv) {

  int i;

  numberOfPeers = argc - 1;

  state = RELEASED;

  sem_init(&mutex, 0, -1);

  for (i=1; i <= numberOfPeers; i++) {
      strcpy(nodes[i-1], argv[i]);
  }

  pp2pInit(SENDPORT, RECVPORT);

  waitForEnter();
  while(1) {

    printf("Mudando o state para WANTED\n");

    state = WANTED;

    responsesWaiting = numberOfPeers;

    timeOwnReq = currentTime;

    broadcast("REQUEST");

    sem_wait(&mutex);

    printf("Mudando o state para HELD\n");
    state = HELD;

    criticalSection();

    printf("Mudando o state para RELEASED\n");
    state = RELEASED;

    for (i = 0; i < requestsQueueCount;i++) {
      sendMessage(requestsQueue[i], "RESPONSE");
      memset(requestsQueue[i], 0, IP_LENGTH);
    }
    requestsQueueCount = 0;
    waitForEnter();
  }
  return 0;
}
