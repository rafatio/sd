#include <stdio.h>
#include <stdlib.h>
#include "perfectp2p.h"
#include "fairp2p.h"

#define SENDPORT 4000
#define RECVPORT 4000

typedef struct Node{
  char ip[30];
  struct Node* next;
} Node;

int hasToken = 1;

Node* front = NULL;
Node* rear = NULL;

void addToQueue(char* ip) {
  Node* node = (Node*)malloc(sizeof(Node));
  strcpy(node->ip, ip);

  if (front == NULL) {
    front = node;
    rear = node;
  }
  else {
    rear->next = node;
    rear = node;
  }

}

void removeFirstFromQueue() {
  if (front == NULL) return;

  Node* node = front;

  front = front->next;

  if (front == NULL) {
    rear = NULL;
  }

  free(node);
}

void pp2pDelivery (char *ip, char *msg) {
  if (0 == strcmp(msg, "REQUEST")) {
    printf("Chegando REQUEST de %s\n", ip);
    if (hasToken) {
      hasToken = 0;
      printf("Enviando TOKEN para %s\n", ip);
      pp2pSend(ip, "TOKEN");
    }
    else {
      printf("Adicionando %s para a fila de REQUEST\n", ip);
      addToQueue(ip);
    }
  } else if (0 == strcmp(msg, "TOKEN")) {
    printf("Recebendo TOKEN de %s\n", ip);
    hasToken = 1;

    if (front != NULL) {
      printf("Enviando TOKEN para %s\n", front->ip);
      hasToken = 0;
      pp2pSend(front->ip, "TOKEN");
      removeFirstFromQueue();
    }
  }
}


int main(int argc, char **argv) {
  pp2pInit(SENDPORT, RECVPORT);

  pthread_exit(NULL);

  return 0;
}
