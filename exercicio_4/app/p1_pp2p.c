
/* Application to experiment the PerfectP2P module */

#include "perfectp2p.h"
#include "fairp2p.h"

#define SENDPORT 3000
#define RECVPORT 4000


// !!! Must be implemented to handle the indication event !!!
void pp2pDelivery (char *src, char *msg) {
  printf("Recebi: %s %s\n", src, msg);
}

// main function
int main(int argc, char **argv) {
  char msg[BUFSIZE];
  int i;

  if(argc < 2)
     error("Digite <prog> <ip dest>");

  pp2pInit(SENDPORT, RECVPORT);

  for(i=0;i<20; i++) {
     sprintf(msg, "Msg: %d", i);
     pp2pSend(argv[1], msg);
  }

  pthread_exit(NULL);
}
