/* Application to experiment the PerfectP2P module */

#include "perfectp2p.h"
#include "fairp2p.h"
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <pthread.h>

#define SENDPORT 3000
#define RECVPORT 3000

#define MAX_NODES 8
#define IP_LENGTH 16
#define START_ELECTION "ELECT"

int id;
int idLeader;
int unknownIds;
int numberOfPeers;
char nodes[MAX_NODES][IP_LENGTH];

void broadcast(char* msg) {
  int i;

  for (i=0; i<numberOfPeers; i++) {
    pp2pSend(nodes[i], msg);
  }
}

void* broadcast_id(void* args) {
  char msg[BUFSIZE];
  sprintf(msg, "%d", id);

  broadcast(msg);

  pthread_exit(NULL);
}

void init_elect() {
  pthread_t thread;

  pthread_create(&thread, NULL, broadcast_id, NULL);
}

// !!! Must be implemented to handle the indication event !!!
void pp2pDelivery (char *src, char *msg) {
  int idMessage;

  // Trata mensagem que inicia a eleição de líder
  if (0 == strcmp(msg, START_ELECTION)) {
    init_elect();
    return;
  }

  unknownIds--;

  idMessage = atoi(msg);

  if (idMessage > idLeader) {
    idLeader = idMessage;
  }

  if (unknownIds == 0) {
    printf("Leader: %d\n", idLeader);
  }
}

// main function
int main(int argc, char **argv) {
  int i;
  int startElection = 0;

  srand(time(NULL));  

  id = rand() % 100;
  printf("Node ID: %d\n", id);

  idLeader = id;

  for (i=1; i<argc; i++) {
    if (0 == strcmp(argv[i], "-s")) {
      startElection = 1;
    } else {
      strcpy(nodes[numberOfPeers], argv[i]);
      numberOfPeers++;
      unknownIds++;
    }
  }

  pp2pInit(SENDPORT, RECVPORT);

  if (startElection) {
    char msg[BUFSIZE];
    strcpy(msg, START_ELECTION);

    printf("Starting election...\n");
    broadcast(msg);
    init_elect();
  }

  pthread_exit(NULL);
}
