// A multithread echo server

#include "mysocket.h"
#include <pthread.h>

#define BUFSIZE 1000
#define NTHREADS 100
#define HOST_MAX_SIZE 100

/* Structure of arguments to pass to client thread */
struct TArgs {
  TSocket cliSock;   /* socket deor for client */
};

struct hostent* getIPByHostname(char* hostname) {
  struct hostent *hn;

  hn = gethostbyname(hostname);

  if (hn == NULL) {
    ExitWithError("GetHostByName() failed");
  }

  return hn;
}


/* Handle client request */
void * HandleRequest(void *args) {

  int read;

  int contentLength;

  char request[BUFSIZE];
  char response[BUFSIZE];

  char host[HOST_MAX_SIZE];

  char *requestAt = request;
  char *responseAt = response;

  struct hostent *hostname;

  TSocket cliSock, srvSock;

  cliSock = ((struct TArgs *) args) -> cliSock;

  free (args);

  read = ReadLine(cliSock, request, BUFSIZE);
  if (read < 0) {
    ExitWithError("ReadLine() failed");
  }

  printf("%s", requestAt);

  // Verifica se o método é GET
  if (strncmp(requestAt, "GET", 3)) {
    strcpy(responseAt, "HTTP/1.1 405 Method Not Allowed");

    if (WriteN(cliSock, responseAt, strlen(request)) < 0) {
      ExitWithError("WriteN() failed");
    }

    close(cliSock);
    pthread_exit(NULL);
  }

  // Verifica se o host está na linha do GET
  if ('/' != requestAt[4]) {
    char *indexOfPath;
    char *hostAt;
    int sizeOfHost;

    //Verifica se a linha começa com "http://" ou "https://"
    if (strncmp(requestAt+4, "http", 4) == 0) {
      hostAt = strstr(requestAt, ":") + 3; // "://"
    } else {
      hostAt = requestAt + 4;
    }

    indexOfPath = strstr(hostAt, "/");
    sizeOfHost = indexOfPath - hostAt;

    strncpy(host, hostAt, sizeOfHost);
    printf("###HOST: %s\n", host);
  }

  // Le parametros do cabeçalho
  do {


      requestAt += read;

      read = ReadLine(cliSock, requestAt, BUFSIZE);
      if (read < 0) {
        ExitWithError("ReadLine() failed");
      }

      printf("%s", requestAt);

      if (0 == strncmp(requestAt, "HOST", 4)) {
        strcpy(host, requestAt+6);
        host[strlen(host)-1] = 0; // Tirando quebra de linha no final do host \n
        host[strlen(host)-1] = 0; // Tirando quebra de linha no final do host \r
        printf("###HOST: %s\n", host);
      }

      //TODO: tratar connection

  } while( strcmp(requestAt, "\n") && strcmp(requestAt, "\r\n") );

  // Resolve IP do HOST e conecta
  hostname = getIPByHostname(host);
  printf("###IP: %s\n", inet_ntoa(*(struct in_addr*)hostname->h_addr));
  srvSock = ConnectToServer(inet_ntoa(*(struct in_addr*)hostname->h_addr), 80);

  // Repassando request para o servidor
  if (WriteN(srvSock, request, strlen(request)) < 0) {
      ExitWithError("WriteN() failed");
  }

  //Lendo cabeçalho da response
  read = 0;
  do {
    responseAt += read;

    read = ReadLine(srvSock, responseAt, BUFSIZE);
    if (read < 0) {
      ExitWithError("ReadLine() failed");
    }

    printf("%s", responseAt);

    if (0 == strncmp(responseAt, "Content-Length", 13)) {
        char contentLengthStr[20];
        strcpy(contentLengthStr, responseAt+15);
        contentLengthStr[strlen(contentLengthStr)-1] = 0; // Tirando quebra de linha no final do contentLengthStr \n
        contentLengthStr[strlen(contentLengthStr)-1] = 0; // Tirando quebra de linha no final do contentLengthStr \r
        contentLength = atoi(contentLengthStr);
    }

  } while( strcmp(responseAt, "\n") && strcmp(responseAt, "\r\n") );

  char *content = (char*) malloc (contentLength+1);
  content[contentLength] = 0;

  if (ReadN(srvSock, content, contentLength) < 0) {
    ExitWithError("ReadN() failed");
  }

  if (WriteN(cliSock, response, strlen(response)) < 0) {
    ExitWithError("WriteN() failed");
  }

  if (WriteN(cliSock, content, strlen(content)) < 0) {
    ExitWithError("WriteN() failed");
  }

  printf("FIM\n");

  close(srvSock);
  close(cliSock);
  pthread_exit(NULL);

}

int main(int argc, char *argv[]) {
  TSocket srvSock, cliSock;        /* server and client sockets */
  struct TArgs *args;              /* argument structure for thread */
  pthread_t threads[NTHREADS];
  int tid = 0;
  fd_set set;  /* file deion set */
  int ret, i;
  char str[BUFSIZE];

  if (argc != 2) { ExitWithError("Usage: server port"); }

  /* Create a passive-mode listener endpoint */
  srvSock = CreateServer(atoi(argv[1]));

  printf("Server read!\n");
  /* Run forever */
  for (;;) {
    /* Initialize the file deor set */
    FD_ZERO(&set);
    /* Include stdin into the file deor set */
    FD_SET(STDIN_FILENO, &set);
    /* Include srvSock into the file deor set */
    FD_SET(srvSock, &set);

    /* Select returns 1 if input available, -1 if error */
    ret = select (FD_SETSIZE, &set, NULL, NULL, NULL);
    if (ret<0) {
       WriteError("select() failed");
       break;
    }

    /* Read from stdin */
    if (FD_ISSET(STDIN_FILENO, &set)) {
      scanf("%99[^\n]%*c", str);
      if (strncmp(str, "FIM", 3) == 0) break;
    }

    /* Read from srvSock */
    if (FD_ISSET(srvSock, &set)) {
      if (tid == NTHREADS) {
        WriteError("number of threads is over");
        break;
      }

      /* Spawn off separate thread for each client */
      cliSock = AcceptConnection(srvSock);

      /* Create separate memory for client argument */
      if ((args = (struct TArgs *) malloc(sizeof(struct TArgs))) == NULL) {
        WriteError("malloc() failed");
        break;
      }
      args->cliSock = cliSock;

      /* Create a new thread to handle the client requests */
      if (pthread_create(&threads[tid++], NULL, HandleRequest, (void *) args)) {
        WriteError("pthread_create() failed");
        break;
      }
    }
  }

  printf("Server will wait for the active threads and terminate!\n");
  /* Wait for all threads to terminate */
  for(i=0; i<tid; i++) {
    pthread_join(threads[i], NULL);
  }
  return 0;
}

