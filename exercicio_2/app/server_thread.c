// A multithread echo server 

#include "mysocket.h"  
#include <pthread.h>

#define BUFSIZE 100
#define NTHREADS 100

/* Structure of arguments to pass to client thread */
struct TArgs {
  TSocket cliSock;   /* socket deor for client */
};

typedef struct User {
  char ip[16];
  char name[20];
  
} User;

typedef struct UserList {
  User *user;
  struct UserList *nextUser;
} UserList;

int users = 0;
 
UserList *userList;

pthread_mutex_t users_mutex;


int addUser(UserList **list, User *user) {
  UserList *ul;
  
  ul = (UserList*) malloc (sizeof(UserList));
  
  ul->user = user;
  
  ul->nextUser = NULL;
  
  pthread_mutex_lock(&users_mutex);
  
  if (users == 0) {
    *list = ul;
  }  
  else {
    UserList *i = *list;
    UserList *j;
    while(i != NULL) {
      if (!strcmp(i->user->name, ul->user->name)) {
        pthread_mutex_unlock(&users_mutex);
        return 0;
      }
      j = i;
      i = i->nextUser;
    }
    j->nextUser = ul;
  }
  users++;
  pthread_mutex_unlock(&users_mutex); 
  return 1;
}

int removeUser(UserList **list, char* username) {
  pthread_mutex_lock(&users_mutex);
  UserList *i = *list;
  UserList *j;
  
  if (i == NULL) {
    pthread_mutex_unlock(&users_mutex);
    return 0;
  }
  
  if (!strcmp(i->user->name, username)) {
    *list = i->nextUser;
    free(i->user);
    free(i);
    users--;
    pthread_mutex_unlock(&users_mutex);
    return 1;
  }
  
  j = i;
  i = i->nextUser;
  
  while (i != NULL) {
    if (!strcmp(i->user->name, username)) {
      j->nextUser = i->nextUser;
      free(i->user);
      free(i);
      users--;
      pthread_mutex_unlock(&users_mutex);
      return 1;
    }
    j = i;
    i = i->nextUser;
  }
  pthread_mutex_unlock(&users_mutex);
  return 0;
}


/* Handle client request */
void * HandleRequest(void *args) {

  TSocket cliSock;
  
  int opr;
  
  int status;
  
  User *user;

  UserList *i;

  char message[100];
  
  char username[20];
  
  /* Extract socket file deor from argument */
  cliSock = ((struct TArgs *) args) -> cliSock;
  free(args);  /* deallocate memory for argument */

  /* Receive the request */
  if (ReadN(cliSock, (char*) &opr, 4) < 0) { 
    ExitWithError("ReadLine() failed"); 
  }
  
  switch (opr) {
    case 1:
    
      user = (User*)malloc(sizeof(User));
      if (ReadN(cliSock, user->name, 20) < 0) { 
        ExitWithError("ReadLine() failed"); 
      }
      
      unsigned int cliLen;
      struct sockaddr_in cliAddr; //estrutura para guardar informacoes do cliente
      memset((void*) &cliAddr, 0, sizeof(cliAddr)); //zera a estrutura de dados
      cliLen = sizeof(cliAddr); //tamanho atual da estrutura de dados
      getpeername(cliSock, (struct sockaddr *) &cliAddr, &cliLen);
      
      strcpy(user->ip, inet_ntoa(cliAddr.sin_addr));
      
      status = addUser(&userList, user);
      
      if (status == 1) {
        printf("Logando usuario: %s %s\n", user->name, user->ip);
      }

      break;
      
    case 2:
      pthread_mutex_lock(&users_mutex);
      i = userList;

      if (WriteN(cliSock, (char*) &users, 4) < 0) {
        ExitWithError("WriteN() failed"); 
      }
      
      while(i != NULL) {
        sprintf(message, "%s %s\n", i->user->ip, i->user->name);
        if (WriteN(cliSock, message, strlen(message)) < 0) { 
          ExitWithError("WriteN() failed"); 
        }
        memset(message,0,strlen(message));
        i = i->nextUser;
      }    
      pthread_mutex_unlock(&users_mutex);
      
      break;
      
    case 3:
      
      if (ReadN(cliSock, username, 20) < 0) { 
        ExitWithError("ReadLine() failed"); 
      }
      
      status = removeUser(&userList, username);

  }

  close(cliSock);
  pthread_exit(NULL);
}

int main(int argc, char *argv[]) {
  TSocket srvSock, cliSock;        /* server and client sockets */
  struct TArgs *args;              /* argument structure for thread */
  pthread_t threads[NTHREADS];
  int tid = 0;
  fd_set set;  /* file deion set */
  int ret, i;
  char str[BUFSIZE];
 
  pthread_mutex_init(&users_mutex, NULL);
 
  if (argc != 1) { ExitWithError("Usage: server"); }

  /* Create a passive-mode listener endpoint */  
  srvSock = CreateServer(2016);

  printf("Server read!\n");
  /* Run forever */
  for (;;) { 
    /* Initialize the file deor set */
    FD_ZERO(&set);
    /* Include stdin into the file deor set */
    FD_SET(STDIN_FILENO, &set);
    /* Include srvSock into the file deor set */
    FD_SET(srvSock, &set);

    /* Select returns 1 if input available, -1 if error */
    ret = select (FD_SETSIZE, &set, NULL, NULL, NULL);
    if (ret<0) {
       WriteError("select() failed"); 
       break;
    }

    /* Read from stdin */
    if (FD_ISSET(STDIN_FILENO, &set)) {
      scanf("%99[^\n]%*c", str);
      if (strncmp(str, "FIM", 3) == 0) break;
    }

    /* Read from srvSock */
    if (FD_ISSET(srvSock, &set)) {
      if (tid == NTHREADS) { 
        WriteError("number of threads is over"); 
        break; 
      }
      
      /* Spawn off separate thread for each client */
      cliSock = AcceptConnection(srvSock);

      /* Create separate memory for client argument */
      if ((args = (struct TArgs *) malloc(sizeof(struct TArgs))) == NULL) { 
        WriteError("malloc() failed"); 
        break;
      }
      args->cliSock = cliSock;

      /* Create a new thread to handle the client requests */
      if (pthread_create(&threads[tid++], NULL, HandleRequest, (void *) args)) { 
        WriteError("pthread_create() failed"); 
        break;
      }
    }
  }
  
  printf("Server will wait for the active threads and terminate!\n");
  /* Wait for all threads to terminate */
  for(i=0; i<tid; i++) {
    pthread_join(threads[i], NULL);
  }
  return 0;
}
