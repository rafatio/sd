// an echo client 
#include "mysocket.h"  

int main(int argc, char *argv[]) {
  TSocket sock;
  char *servIP;                /* server IP */
  unsigned short servPort;     /* server port */
  int n = 3;

  char username[20];

  if (argc != 2) {
    ExitWithError("Usage: client <remote server IP>");    
  }

  servIP = argv[1];
  servPort = 2016;

    /* Create a connection */
    sock = ConnectToServer(servIP, servPort);

    if (WriteN(sock, (char*)&n, 4) <= 0) { 
      ExitWithError("WriteN() failed"); 
    }  
    
    scanf(" %[^\n]", username);  
    
    if (WriteN(sock, username, 20) <= 0) { 
      ExitWithError("WriteN() failed"); 
    }
   

  return 0;
}
