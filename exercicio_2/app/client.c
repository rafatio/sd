// an echo client 
#include "mysocket.h"  

int main(int argc, char *argv[]) {
  TSocket sock;
  char *servIP;                /* server IP */
  unsigned short servPort;     /* server port */
  int n = 2;
  int users;
  int i;
  char message[100];
  char cmd;

  if (argc != 2) {
    ExitWithError("Usage: client <remote server IP>");    
  }

  servIP = argv[1];
  servPort = 2016;

  do {

    /* Create a connection */
    sock = ConnectToServer(servIP, servPort);

    if (WriteN(sock, (char*)&n, 4) <= 0) { 
      ExitWithError("WriteN() failed"); 
    }
   
    if (ReadN(sock, (char*)&users, 4) < 0) { 
      ExitWithError("ReadN() failed");
    } 
    
    for ( i = 0; i < users; i++ ) {
      if (ReadLine(sock, message, 100) < 0) { 
        ExitWithError("ReadN() failed");
      }
      printf("%s", message);
    }
     
    close(sock);
    
    printf("Pressione Enter para atualizar a lista de usuarios.\n");
    printf("Caso deseje sair, digite 'q' e pressione Enter.\n");
    scanf("%c", &cmd);
  } while (cmd != 'q');
  return 0;
}
